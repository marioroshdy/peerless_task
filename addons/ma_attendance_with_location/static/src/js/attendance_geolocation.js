odoo.define('hr_attendance_geolocation.attendances_geolocation', function (require) {
    "use strict";

    var MyAttendances = require('hr_attendance.my_attendances');

    MyAttendances.include({

            update_attendance: function () {
                var self = this;

                if (navigator.geolocation) {
                    navigator.geolocation.getCurrentPosition(
                      (position) => {
                        const pos = {
                          lat: position.coords.latitude,
                          lng: position.coords.longitude
                        };


                        var location_url = 'http://maps.google.com/maps?q=' + pos.lat + ',' + pos.lng;
                        console.log(location_url);

                        this._rpc({
                            model: 'hr.employee',
                            method: 'attendance_manual',
                            args: [[self.employee.id], 'hr_attendance.hr_attendance_action_my_attendances', false, location_url],
                        })
                        .then(function(result) {
                            if (result.action) {
                                self.do_action(result.action);
                            } else if (result.warning) {
                                self.do_warn(result.warning);
                            }
                        });

                      },
                    );
                  }


                },

            });


});
