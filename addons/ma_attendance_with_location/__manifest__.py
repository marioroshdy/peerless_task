# -*- coding: utf-8 -*-
{
    'name': "Attendance Location",

    'author': "Mario Roshdy <marioroshdyn@gmail.com>",
    'website': "",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/13.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'hr',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base', 'hr', 'hr_attendance'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'views/hr_attendance_views.xml',
        'views/assets.xml',

    ],

}